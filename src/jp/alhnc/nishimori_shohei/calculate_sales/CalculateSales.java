package jp.alhnc.nishimori_shohei.calculate_sales;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {
		try{
			Map<String, Long> addingup = new HashMap<>();				//支店コード, 売上額 map作成
            Map<String, String> branchoffice = new HashMap<>();			//支店コード, 支店名 map作成

            BufferedReader br = null;
			try{
				File branchfile = new File(args[0],"branch.lst"); 				 //支店定義ファイル用意
				if(!branchfile.exists()){											 //支店定義ファイルが存在するか
					System.out.println("支店定義ファイルが存在しません");		//定義ファイルが存在しない場合
                    return;
                }
                br = new BufferedReader(new FileReader(branchfile));
                String oneline;								 			 //1行読み込んだテキストの格納先作成
                while((oneline = br.readLine()) != null){    		 //1行毎の定義データの読み込み
                    String[] data = oneline.split(",", -1);		 		 //,を境に文字列を分割
                    if(data[0].matches("^\\d{3}$") || data.length != 2){ //dataの要素数が２の時かつ\\d{3}3桁指定 ,(コンマ)を付け(?!,).+で1文字以上の,(コンマ)を含まない任意の文字列
                        System.out.println("支店定義ファイルのフォーマットが不正です");	//定義ファイルの中身が指定通りでない場合
                        return;															//returnにて終了
                    }
                    String code = data[0];					 		 //支店コードをcodeに代入
                    String name = data[1];					 		 //支店名をsitenに代入
                    codelist.add(code);								 //支店コードをリスト格納,出力作成のforループに使用
                    addingup.put(code,0L);					 		 //支店コードと数字(0LはLong型の数字0という意味)
                    branchoffice.put(code, name);
                }
			}finally{
				if (br != null) br.close();								//支店定義ファイルを閉じる
			}
			FilenameFilter filter = new FilenameFilter() {					 	//.rcdかつ8桁が含まれるフィルタの作成
				public boolean accept(File dir, String filename){ 				 	//指定したファイルをリストに含めるか判定
					return new File(dir, filename).isFile() && filename.matches("^\\d{8}.rcd$");  						 	//.rcd、8桁数字(.rcdを含む12文字制限)のフィルタ
				}
			};

			BufferedReader br2 = null;
			try{
				File[] earningsfiles = new File(args[0]).listFiles(filter);				//".rcd"+"8桁数字の条件フィルタを作成する
                int min = Integer.parseInt(earningsfiles[0].getName().substring(0, 8));				//00000001.rcdを取り出し
                int max = Integer.parseInt(earningfiles[arningfiles.length - 1].getName().substring(0, 8));
                if (min + earningsfiles.length - 1 != max) {
                    System.out.println("売上ファイル名が連番になっていません");
                    return;
                }

				for(int i=0;i<earningsfiles.length;i++){					  		 		//file2の数だけforループを行う
					br2 = new BufferedReader(new FileReader(earningsfiles[i]));
                    String code = br2.readLine();				  				//支店コードをcodeに代入
                    if(!branchoffice.containsKey(code)){											//codeが定義で読取したcodeリスト内と一致しているかどうか
                        System.out.println(earningsfiles[i]+"の支店コードが不正です");		//リスト内にcodeがなかった場合
                        return;
                    }
                    long earningl = Long.parseLong(br2.readLine()); 			//売上を合算する為にLong型(売上金額は大きいと想定）に変換
                    String threelines = earnings.readLine();							//エラー処理の為に3行目がないか読み込み
                    if(threelines != null){										//3行目のデータがあったかどうか確認
                        System.out.println(earningsfiles[i]+"のフォーマットが不正です");	//3行目にデータがあった場合
                        return;
                    }
                    Long total = addingup.get(code) + earningl;
                    if(total.toString().length() > 10){				//goukeiをString型にしてlengthで桁取得し合計金額が10桁を超えているかどうか
                        System.out.println("合計金額が10桁を超えました");			//gassanに売上額を合算
                        return;
                    }
                    addingup.put(code,total);//売上を合算
				}
			}finally{
				earnings.close();																			//売上ファイルを閉じる
			}
			PrintWriter pw = null;																		//支店別集計ファイル(出力)の作成
			try{
				File branch = new File(args[0],"branch.out");											//kadaiにbranch.outを作成準備
                pw = new PrintWriter(branch);															//branchで新しいファイルを作成する
                for(String code : branchoffice.keySet()){
                    pw.println(code+","+branchoffice.get(code)+","+addingup.get(code));
				}
			}finally{
				pw.close();																				//中身の作成を終了しファイルを作成
			}

		}catch(IOException e){
            System.out.println("予期せぬエラーが発生しました");
            return;
		}
	}
}
